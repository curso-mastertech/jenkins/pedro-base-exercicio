FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/Api-Investimentos-0.0.1-SNAPSHOT.jar

WORKDIR /opt/app

COPY ${JAR_FILE} app1.jar

ENTRYPOINT ["java","-jar","app1.jar"]