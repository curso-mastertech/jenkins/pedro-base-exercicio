package com.br.investimentos.Api.Investimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class ApiInvestimentosApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ApiInvestimentosApplication.class, args);
    }

}
